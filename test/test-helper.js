/**
 * Created by Rustam <rustam.bmt@gmail.com> on 01.02.2017.
 */

import chai from 'chai';
import chaiEnzyme from 'chai-enzyme';
import {assert} from 'chai';
import {User} from '../server/models/User';

var chaiImmutable = require('chai-immutable');
chai.use(chaiImmutable);
chai.use(chaiEnzyme()) // Note the invocation at the end

delete process.env['DEBUG_FD'];

const mongoose = require('../server/mongoose').connect('test');

var connectedStatus = false;
mongoose.connection.on('connected', function (err) {
	assert.ifError(err);
	User.remove( {},()=> {
		connectedStatus = true;
	});
});
mongoose.connection.on('error', function (err) {
	assert.ifError(err);
	connectedStatus = true;
});

function checkMognoDbConnection() {
	if( connectedStatus !== true )
		setTimeout( checkMognoDbConnection, 500 );
}

checkMognoDbConnection();
