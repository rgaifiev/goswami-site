
import im from 'immutable'

export const SET_USER                 = "SET_USER";
export const SET_USER_LOGGING_STATUS  = "SET_USER_LOGGING_STATUS";

export function loginUser(userName, password) {
	return (dispatch, getState, api ) => {

		setUserLogging( dispatch, {
			logging: true
		});

		return api.loginUser(userName, password).then(
			userDetail => {
				if( userDetail ) {
					dispatch({
						type: SET_USER,
						user: im.fromJS(userDetail),
						status: im.fromJS({logging: false}),
					})
				} else {
					setUserLogging( dispatch, {logging: false, error: ' User is not found'} );
				}
			},
			error => {
				api.log.error(error);
				setUserLogging( dispatch, {logging: false, error: ' User is not found'} );
			}
		)
	}
}

function setUserLogging( dispatch, status ) {
	dispatch({
		type: SET_USER_LOGGING_STATUS,
		status: im.fromJS( status )
	});
}
