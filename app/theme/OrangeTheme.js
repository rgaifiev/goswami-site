/**
 * Created by rgaifiev_add on 27.01.2017.
 */

import * as colors from 'material-ui/styles/colors';
import {fade} from 'material-ui/utils/colorManipulator';

const OrangeTheme = {
    palette: {
	    primary1Color: colors.orange300,
	    primary2Color: colors.orange500,
	    primary3Color: colors.grey400,
	    accent1Color: colors.pinkA200,
	    accent2Color: colors.grey100,
	    accent3Color: colors.grey500,
	    textColor: colors.darkBlack,
	    alternateTextColor: colors.white,
	    canvasColor: colors.white,
	    borderColor: colors.grey300,
	    disabledColor: fade(colors.darkBlack, 0.3),
	    pickerHeaderColor: fade(colors.fullWhite, 0.12),
	    clockCircleColor: fade(colors.orange500, 0.07),

	    shadowColor: colors.fullBlack,
    },
    appBar: {
        color: colors.amber800,
    },
		checkbox: {
			boxColor: colors.orange800,
		}
};

export default OrangeTheme;