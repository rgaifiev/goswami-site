/**
 * Rustam Gaifiev
 * rustam.bmt@gmail.com
 */

import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory'
import Root from './pages/Root'
import {initStore} from './store'
import {Provider} from 'react-redux'

require('./utils/injectTapEventPlugin');

const history = createBrowserHistory()
const store = initStore();

ReactDOM.render(
    <BrowserRouter history={history}>
	    <Provider store={store}>
		    <Root translations={window.translations}/>
	    </Provider>
    </BrowserRouter>,
    document.getElementById('content-root')
);
if (module.hot) {
	module.hot.accept()
}
