
//import {createSelector} from 'reselect'

export const getUserDetails = (state) => state.getIn(['user', 'details']);
export const getLoggingStatus = (state) => state.getIn(['user', 'status']);
