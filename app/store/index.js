
import im from 'immutable';
import { combineReducers } from 'redux-immutablejs';
import { compose, createStore, applyMiddleware } from 'redux';
import think from 'redux-thunk';
import createLogger from 'redux-logger';
import ClientApi from '../api'

import user from './userReducer';

export const theReducer = combineReducers(im.Map({
	user
}));

const initialState = {};

const browserHasDevTools = typeof window === 'object' && typeof window.devToolsExtension !== 'undefined';

export function initStore(api = new ClientApi(), state = initialState) {
	let store = createStore(
		theReducer,
		im.fromJS(state),
		compose(
			applyMiddleware(think.withExtraArgument( api ), createLogger()),
			browserHasDevTools ? window.devToolsExtension() : f => f
		)
	);

	if (module.hot) {
		// Enable Webpack hot module replacement for reducers
		module.hot.accept('../store', () => {
			const nextRootReducer = require('../store/index');
			store.replaceReducer(nextRootReducer);
		});
	}

	return store;
}