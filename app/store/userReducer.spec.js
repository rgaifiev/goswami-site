import {expect, assert} from 'chai';
import {initStore} from '../store'
import {loginUser} from '../actions/user'
import ClientApi from '../api'
import FetchLocalApi from '../api/FetchLocalApi'
import * as selector from './selector'

require('regenerator-runtime/runtime');

describe( 'User reducer', () => {

	var api = new ClientApi( new FetchLocalApi() );

	it( 'login user failed', (done) => {
		let store = initStore( api );

		var stage = 0;
		store.subscribe( () => {
			var state = store.getState();
			var status = selector.getLoggingStatus(state);
			switch( stage ) {
				case 0:
					expect(status).to.have.property('logging', true);
					break;
				case 1:
					expect(status).to.have.property('logging', false);
					expect(status).to.have.property('error');
					done();
					break;
			}
			++stage;
		});

		store.dispatch( loginUser('username', 'password'));
	});

});