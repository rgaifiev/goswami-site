
import im from 'immutable';

const act = require('../actions/user');
const initialState = im.Map({
	details: im.Map({}),
	status: im.Map({
		logging: false,
		error: null
	}),
});

export default function reducer(state = initialState, action) {
	switch (action.type) {
		case act.SET_USER: {
			state = state.set('details', action.user);
			if( action.status )
				state = state.set('status', action.status);
			break;
		}
		case act.SET_USER_LOGGING_STATUS: {
			state = state.set('status', action.status);
			break;
		}
	}

	return state;
}
