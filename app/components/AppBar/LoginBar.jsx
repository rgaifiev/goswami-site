
import React from 'react';
import {Link} from 'react-router-dom';

import * as colors from 'material-ui/styles/colors';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import {MenuItem, Paper} from 'material-ui';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import NavigationClose from 'material-ui/svg-icons/navigation/close';

const RightLoginBarMenu = (props) => (
	<IconMenu
		{...props}
		iconButtonElement={
			<IconButton><MoreVertIcon /></IconButton>
		}
		targetOrigin={{horizontal: 'right', vertical: 'top'}}
		anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
	>
		<MenuItem primaryText={props.tr.registerNow} />
		<MenuItem primaryText={props.tr.forgotPassword} />
	</IconMenu>
);

RightLoginBarMenu.muiName = 'IconMenu';

class LoginBar extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		const tr = this.context.tr.loginPage;

		return <div>
			<AppBar
				title={tr.loginTitle}
				style={this.props.error?{background: colors.red500}:{}}
				iconElementLeft={<IconButton><NavigationClose /></IconButton>}
				iconElementRight={<RightLoginBarMenu tr={tr} />}
			/>
			{this.props.error &&
				<Paper style={{width: '100%', height: '50px', background: colors.grey300, padding: '8px 8px 8px 8px'}} zDepth={2}>
					<span style={{fontSize:'14px', color: colors.red500}}>{this.props.error}</span>
				</Paper>
			}
		</div>
	}
}

LoginBar.contextTypes = {
	tr: React.PropTypes.object,
	error: React.PropTypes.string
};

export default LoginBar