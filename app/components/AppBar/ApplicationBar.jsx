
import React from 'react';
import {Link} from 'react-router-dom';

import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import Divider from 'material-ui/Divider';

const RightAppBarMenu = (props) => (
	<IconMenu
		{...props}
		iconButtonElement={
			<IconButton><MoreVertIcon /></IconButton>
		}
		targetOrigin={{horizontal: 'right', vertical: 'top'}}
		anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
	>
		<MenuItem primaryText="Refresh" />
		<MenuItem primaryText="Help" />
		<Divider />
		<MenuItem primaryText="Sign in" containerElement={<Link to="/login"/>} />
	</IconMenu>
);

RightAppBarMenu.muiName = 'IconMenu';

class ApplicationBar extends React.Component {
    constructor(props) {
        super(props);

	    this.state = {
		    logged: true,
	    };
    }

	handleChange = (event, logged) => {
		this.setState({logged: logged});
	};

    render() {
        return <AppBar
				    title={this.context.tr.defaultTitle}
				    iconElementRight={<RightAppBarMenu />}
			    />
    }
}

ApplicationBar.contextTypes = {
    tr: React.PropTypes.object
};

export default ApplicationBar