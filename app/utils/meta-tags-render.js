
const default_props = {
    defaultTitle: null,
    titleDelimiter: ' | '
}

class MetaTagsRender {

    constructor( props ) {
        this.props = Object.assign(default_props, props);
        this.title = null;
    }

    /*
    *   Common render method
    */
    render() {
        if( typeof document !== 'undefined' ) {
            var head = document.getElementsByTagName('head')[0];
            this.renderTitle( head );
        }
    }

    /*
    *   Title rendering
    */
    getTitle() {
        return this.props.defaultTitle ? this.props.defaultTitle + this.props.titleDelimiter + this.title : this.title;
    }

    renderTitle( root ) {
        var titleNode = root.querySelector('title');
        if( ! titleNode ) {
            titleNode = document.createElement('title');
            root.insertBefore( titleNode, null );
        }
        const newTitle = this.getTitle();
        if( newTitle !== titleNode.innerHTML )
            titleNode.innerHTML = newTitle;
    }
}

export function getMetaTagsRender (props) {
    if (!metaTagesRender) {
        metaTagesRender = new MetaTagsRender(props);
    }

    return metaTagesRender;
}

var metaTagesRender = null;
