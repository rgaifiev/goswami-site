import React, {PropTypes} from 'react';
import {ApplicationBar} from '../components';

class MainPage extends React.Component {

	constructor(props) {
		super(props);
	}

	render() {
		return <ApplicationBar/>
	}
}

MainPage.contextTypes = {
	tr: React.PropTypes.object
};

export default MainPage;
