import React, {PropTypes} from 'react';
import injectSheet from 'react-jss'
import {Link} from 'react-router-dom'
var ImmutablePropTypes = require('react-immutable-proptypes');

import * as colors from 'material-ui/styles/colors';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import {TextField, RaisedButton, FlatButton, Checkbox, SvgIcon, CircularProgress, AppBar, IconButton} from 'material-ui';

import LoginBar from '../components/AppBar/LoginBar'

import {connect} from 'react-redux';
import {loginUser} from '../actions/user';
import {getLoggingStatus} from '../store/selector'

const sizeLoginPane = {
	w: 320,
	h: 480
};

const styles = {
	bkgSheet: {
		background: '#F9F9F9',
		width: '100%',
		height:'100%',
		position: 'absolute',
		left: 0,
		top: 0,
    fontFamily: 'Roboto',
		fontSize: '12px'
	},
	cardStyle: {
		width: sizeLoginPane.w + 'px',
		height: sizeLoginPane.h + 'px',
		position: 'absolute',
		top: '50%',
		marginTop: '-' + sizeLoginPane.h/2 + 'px',
		left: '50%',
		marginLeft: '-' + sizeLoginPane.w/2+ 'px',
		padding: '0px 0px 0px 0px'
	},
	flatButton: {
		width: '100%'
	},
	loginLinks: {
		padding: '20px 10px 5px 10px',
		display: 'block'
	},
	fontLink: {
		color: '#00bcd4',
		fontSize: '13.5px',
		fontWeight: 'normal'
	},
	waitingContainer: {
		width: 'inherit',
		height: 'inherit'
	}
}

const FacebookIcon = (props) => (
	<SvgIcon {...props}>
		<path fill="#3C5A99" d="M22.7,24c0.7,0,1.3-0.6,1.3-1.3V1.3C24,0.6,23.4,0,22.7,0H1.3C0.6,0,0,0.6,0,1.3v21.4 C0,23.4,0.6,24,1.3,24L22.7,24L22.7,24z"/>
		<path fill="#FFFFFF" d="M16.5,24v-9.3h3.1l0.5-3.6h-3.6V8.8c0-1,0.3-1.8,1.8-1.8h2V3.7c-0.4,0-1.5-0.2-2.8-0.2 c-2.8,0-4.7,1.7-4.7,4.8V11h-3v3.6h3.2V24H16.5z"/>
	</SvgIcon>
);

const GoogleIcon = (props) => (
	<SvgIcon {...props}>
		<g id="layer1" transform="translate(-373.64 -318.34)">
			<path id="rect1942" fill="#DD4B38" d="M394,318.3h-16.8c-2,0-3.6,1.6-3.6,3.6v16.8c0,2,1.6,3.6,3.6,3.6H394c2,0,3.6-1.6,3.6-3.6V322 C397.6,320,396,318.3,394,318.3z"/>
			<linearGradient id="path1950_1_" gradientUnits="userSpaceOnUse" x1="-414.9614" y1="88.3507" x2="-420.2436" y2="103.7467" gradientTransform="matrix(-0.5915 0 0 0.5915 134.2882 262.5531)">
				<stop  offset="0" style={{stopColor:'#FFFFFF'}}/>
				<stop  offset="1" style={{stopColor:'#FFFFFF', stopOpacity:'0'}}/>
			</linearGradient>
			<path id="path1950" fill="url(#path1950_1_)" d="M391.3,319.4H380c-3,0-5.4,2.4-5.4,5.5v11c0.1,2.4,0.5,0.9,1.2-1.7c0.8-3,3.5-5.7,6.8-7.7	c2.5-1.5,5.3-2.5,10.4-2.6C395.8,323.8,395.6,320.1,391.3,319.4z"/>
		</g>
		<polygon id="_x2B__3_" fill="#FFFFFF" points="17.7,1.8 19.6,1.8 19.6,6.2 24,6.2 24,8.1 19.6,8.1 19.6,12.5 17.7,12.5 17.7,8.1 13.3,8.1 13.3,6.2 17.7,6.2 "/>
		<linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="-84.1891" y1="-295.492" x2="-84.1891" y2="-317.5676" gradientTransform="matrix(1 0 0 1 91 319.5)">
			<stop  offset="0" style={{stopColor:'#F2F2F2'}}/>
			<stop  offset="0.2413" style={{stopColor:'#F6F6F6'}}/>
			<stop  offset="1" style={{stopColor:'#FFFFFF'}}/>
		</linearGradient>
		<path fill="url(#SVGID_1_)" d="M10,17.1l-1.3-1c-0.4-0.3-0.9-0.7-0.9-1.5c0-0.8,0.5-1.3,1-1.7c1.5-1.2,2.8-2.4,2.8-5c0-2.4-1.3-3.7-2.1-4.4	c0.3,0,2.1,0,2.1,0l2-1.6H6.3c-1.8,0-4,0.3-6,2C0.2,4,0.1,4.1,0,4.1v8.5C1,13.4,2.3,14,3.8,14c0.3,0,0.7,0,1.1-0.1 c-0.2,0.4-0.3,0.8-0.3,1.4c0,1.1,0.6,1.7,1,2.4c-1.3,0.1-4.1-0.1-5.6,1.1c0,0,0,1.1,0,1.5s0,0.5,0,0.5c0.7-1,1.8-1.4,2.3-1.6	c1.5-0.5,3.3-0.6,3.7-0.6c0.3,0,0.5,0,0.8,0c2.6,1.9,4.5,2.9,4.6,4.6c0,0.3-0.1,0.5-0.1,0.6h2c0.2-0.5,0.3-1.1,0.3-1.7 C13.6,19.9,11.6,18.4,10,17.1z M5.6,12.7c-3,0-4.2-3.5-4.2-5.8C1.4,5.9,1.5,5,2,4.3c0.6-0.7,1.5-1.2,2.4-1.2c2.9,0,4.3,3.9,4.3,6.4 c0,0.5-0.3,1.5-0.9,2.2C7.3,12.4,6.5,12.7,5.6,12.7z"/>
	</SvgIcon>
);

const VkIcon = (props) => (
	<SvgIcon {...props}>
		<g id="layer1">
			<g id="g3257">
				<path fill="#4C75A3" d="M3.7,0h16.6c2,0,3.7,1.7,3.7,3.7v16.6c0,2-1.7,3.7-3.7,3.7H3.7c-2,0-3.7-1.7-3.7-3.7V3.7 C0,1.7,1.7,0,3.7,0"/>
				<path fill="#FFFFFF" d="M11.8,16.8h1c0,0,0.3,0,0.4-0.2c0.1-0.1,0.1-0.4,0.1-0.4s0-1.3,0.6-1.5 c0.6-0.2,1.4,1.3,2.2,1.8c0.6,0.4,1.1,0.3,1.1,0.3l2.2,0c0,0,1.1-0.1,0.6-1c0-0.1-0.3-0.7-1.6-1.9c-1.4-1.3-1.2-1.1,0.5-3.3 c1-1.3,1.4-2.2,1.3-2.5c-0.1-0.3-0.9-0.2-0.9-0.2l-2.5,0c0,0-0.2,0-0.3,0.1c-0.1,0.1-0.2,0.3-0.2,0.3s-0.4,1-0.9,1.9 c-1.1,1.9-1.5,2-1.7,1.9c-0.4-0.3-0.3-1.1-0.3-1.7c0-1.8,0.3-2.6-0.5-2.8c-0.3-0.1-0.5-0.1-1.2-0.1c-0.9,0-1.6,0-2,0.2 C9.3,7.8,9.1,8.1,9.2,8.1c0.2,0,0.5,0.1,0.7,0.4c0.3,0.3,0.2,1.1,0.2,1.1s0.1,2.1-0.3,2.4c-0.3,0.2-0.8-0.2-1.8-1.9 C7.6,9.3,7.2,8.3,7.2,8.3S7.2,8.1,7,8C6.9,7.9,6.6,7.9,6.6,7.9l-2.3,0c0,0-0.4,0-0.5,0.2c-0.1,0.1,0,0.4,0,0.4s1.8,4.3,3.9,6.5 C9.6,16.9,11.8,16.8,11.8,16.8"/>
			</g>
		</g>
	</SvgIcon>
);

/**
 *  LoginPage class
 */
class LoginPage extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			username: '',
			password: ''
		}
	}

	handleLogin = () => {
		this.props.dispatch(loginUser(this.state.username, this.state.password ));
	}

	handleOnChangeValue = (event, value) => {
		var state = {};
		state[event.target.id] = value;
		this.setState(state);
	}

	render() {
		const logging = this.props.status.get('logging') || false;

		return <div className={this.props.classes.bkgSheet}>
			{ this.props.logging ? this.renderWaitForLogin() : this.renderLoginForm() }
		</div>
	}

	renderWaitForLogin() {
		//TODO: input beautiful info 'loging processing...'
		return <div className={this.props.classes.waitingContainer}>
			<CircularProgress size={100} thickness={5} style={{top: '40%', left:'46%'}} />
		</div>
	}

	renderLoginForm() {
		const tr = this.context.tr.loginPage;
		const errorText = this.props.status.get('error');
		const h = errorText ? sizeLoginPane.h + 50 : sizeLoginPane.h;

		console.log( this.props.status );
		return <Card
			className={this.props.classes.cardStyle}
			style={{boxShadow: '0 16px 28px 0 rgba(0,0,0,0.22), 0 25px 55px 0 rgba(0,0,0,0.21)', height: h+'px' }}
		>
			<LoginBar error={errorText}/>

			<CardHeader
				subtitle={tr.loginSubtitle}
				textStyle={{paddingRight: 0}}
				style={{paddingBottom: 0}}
			/>
			<CardText
				style={{paddingTop: 0, paddingBottom: '5px'}}
			>
				<TextField
					floatingLabelText={tr.userName}
					fullWidth={true}
				  id="username"
					value={this.state.username}
				  onChange={this.handleOnChangeValue}
				/><br/>
				<TextField
					floatingLabelText={tr.password}
					fullWidth={true}
					id="password"
					onChange={this.handleOnChangeValue}
				/>
				<Checkbox label={tr.remember} style={{paddingTop: '10px'}}/>
			</CardText>
			<CardActions>
				<RaisedButton label={tr.loginButton} fullWidth={true} buttonStyle={{height:'35px'}} style={{marginBottom: '15px'}} primary={true} onClick={this.handleLogin}/>
				<FlatButton label={tr.loginFacebook} className={this.props.classes.flatButton} labelStyle={{textTransform: 'none'}} icon={<FacebookIcon />} />
				<FlatButton label={tr.loginGoogle} className={this.props.classes.flatButton} labelStyle={{textTransform: 'none'}} icon={<GoogleIcon />}/>
				<FlatButton label={tr.loginVK} className={this.props.classes.flatButton} labelStyle={{textTransform: 'none'}} icon={<VkIcon/>}/>
			</CardActions>
		</Card>
	}
}

LoginPage.contextTypes = {
	tr: React.PropTypes.object,
	muiTheme: React.PropTypes.object,
	status: ImmutablePropTypes.map
};

const mapStateToProps = (state) => {
	return {
		status: getLoggingStatus(state)
	}
};

export default connect(mapStateToProps)(injectSheet(styles)(LoginPage))
