
import React, {PropTypes} from 'react';
import {DefaultTheme} from '../theme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {getMetaTagsRender} from '../utils/meta-tags-render'
import {Route, Switch} from 'react-router-dom';

import getMuiTheme from 'material-ui/styles/getMuiTheme';

import LoginPage from './Login'
import MainPage from './Main'

let metaTags = getMetaTagsRender({
    defaultTitle: 'Goswami.ru'
});

const NoMatch = () => (
  <div>
    NOT_MATCH_01234567
  </div>
)
class Root extends React.Component {
    constructor(props) {
        super(props);
        this.theme = this.props.theme || getMuiTheme(DefaultTheme);
    }

    getChildContext() {
        return {
            tr: this.props.translations,
            muiTheme: this.theme
        };
    }

    DefaultPage = (props) => {
      return <MainPage/>
    }

    Login = (props) => {
      return <LoginPage/>
    }


	render() {
      metaTags.title = this.props.translations.pageTitle;
	    return (<MuiThemeProvider muiTheme={this.theme}>
          <Switch>
              <Route exact path="/" render={this.DefaultPage} />
              <Route exact path="/login" render={this.Login} />
              <Route component={NoMatch}/>
          </Switch>
      </MuiThemeProvider>);
    }

    componentDidUpdate() {
        metaTags.render();
    }
}

Root.childContextTypes = {
    tr: React.PropTypes.object,
	  muiTheme: React.PropTypes.object
};

Root.propTypes = {
    theme: PropTypes.object,
    translations: PropTypes.object.isRequired
}

export default Root;