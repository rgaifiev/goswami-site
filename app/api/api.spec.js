import {expect, assert} from 'chai';
import ClientApi from '../api'
import FetchLocalApi from './FetchLocalApi'

require('regenerator-runtime/runtime');

describe( 'API loginUser', () => {

	var api = new ClientApi( new FetchLocalApi() );

	it( 'login user failed', (done) => {
		api.loginUser('username', 'password').then(
			u => {
				expect(u).to.be.a('null');
				done();
			},
			error => {
				assert.ifError(error);
				done();
			}
		);
	});

});