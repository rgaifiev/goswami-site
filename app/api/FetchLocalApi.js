
import * as api from '../../server/services/api'

export default class FetchLocalApi {

	constructor() {
	}

	async post(url, query = {}) {
		var result = null;

		if( url === '/api/login' ) {
			result = await api.login( query.u, query.p );
		} else
			throw new Error( 'Unknown request url', url );

		return result;
	}
}