
import FetchApi from './FetchApi';

export default class ClientApi {
	constructor(fetchApi, log) {
		this.log = log || console;
		this.fetch = fetchApi || new FetchApi('');
	}

	async loginUser( username, password ){
		const response = await this.fetch.post('/api/login', {
			u: username,
			p: password
		});
		return response;
	}
};