import fetch from 'isomorphic-fetch';
import { format as format_url, parse as parse_url } from 'url';
import { extend, merge as mergeObj } from 'lodash';

export default class FetchApi {

	constructor(host) {
		this.host = host;
	}

	getApiUrl(relativeUrl) {
		return this.host + relativeUrl;
	}

	apiUrlForFetch(relativeUrl, query = {}) {
		const urlObj = parse_url(this.getApiUrl(relativeUrl));
		urlObj.query = mergeObj(urlObj.query, query);

		return format_url(urlObj);
	}

	// async get(relativeUrl, query = {}) {
	// 	const defaultHeaders = {};
	//
	// 	const response = await fetch(
	// 		this.apiUrlForFetch(relativeUrl, query),
	// 		{
	// 			credentials: 'same-origin',
	// 			headers: defaultHeaders
	// 		}
	// 	);
	//
	// 	await this.handleResponseError(response);
	//
	// 	return response;
	// }

	async post(relativeUrl, query = {}) {
		const defaultHeaders = {};
		const url = this.getApiUrl(relativeUrl)
		const response = await fetch(
			url,
			{
				credentials: 'include',
				method: 'post',
				headers: {
					"Accept": "application/json",
					"Content-Type": "application/json"
				},
				body: JSON.stringify(query)
			}
		);

		await this.handleResponseError(response);

		if( response.status === 204 )
			return null;

		return await response.json();
	}

	async handleResponseError(response) {
		if (!response.ok) {
			let json, errorMessage;

			errorMessage = response.statusText;

			try {
				json = await response.json();
				errorMessage = json.error || errorMessage;
			} catch (e) {
				throw new Error(errorMessage);
			}

			throw new Error(errorMessage);
		}
	}
}