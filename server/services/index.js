
import * as api from './api';

const Router = require('koa-router');
const koaBody = require('koa-body')();
const router = new Router();

router.post('/api/login', koaBody, async (ctx, next) => {
	const username = ctx.request.body.u;
	const password = ctx.request.body.p;
	const user = await api.login( username, password );
	if( user ) {
		ctx.body = user;
	} else {
		ctx.body = null;
	}
});

export const route = router.routes();
