
import {User} from '../models/User'

export async function login( username, password) {
	const user = await User.authorize( username, password );
	return user;
}
