
'use strict';

/**
 * Module dependencies.
 */

require('regenerator-runtime/runtime');

const resolve = require('path').resolve;
const assert = require('assert');
const debug = require('debug')('koa-static');
const send = require('koa-send');

/**
 * Serve static files from `root`.
 *
 * @param {String} root
 * @param {Object} [opts]
 * @return {Function}
 * @api public
 */
module.exports = (root, opts) => {
    opts = opts || {};

    assert(root, 'root directory is required to serve files');

    // options
    //debug('static "%s" %j', root, opts);
    opts.root = resolve(root);
    if (opts.index !== false) opts.index = opts.index || 'index.html';

    return async (ctx, next) => {
        const f = ctx.params[opts.param];
        if( f ) {
            debug('request %s', f);
            if (ctx.method == 'HEAD' || ctx.method == 'GET') {
                if( opts.handler )
                    await sendContent( ctx, f, opts.handler(f) );
                else
                    await send(ctx, f, opts);
            } else {
                debug('wrong method %s', ctx.method);
                await next();
            }
        } else
            await next();
    };
}

const sendContent = async (ctx, file, content) => {
    ctx.set('Connection', 'keep-alive');
    ctx.set('Content-Type', 'application/octet-stream');
    ctx.set('Content-Length', content.length );
    ctx.body = content;
}