
import {expect, assert} from 'chai';
import {User} from './User';
require('regenerator-runtime/runtime');

describe( 'Model -> User', () => {

	it( 'create new user', (done) => {

		const mongoose = require('../mongoose').mongoose;

		const TEST_USER = {
			username: 'Test1',
			password: 'password1'
		}

		var user = new User( TEST_USER );
		expect(user.get('password')).to.exist;
		expect(user.checkPassword('password')).to.be.false;

		user.save( (err) => {
			assert.ifError(err);
			User.find( {username: TEST_USER.username}, (err, u) => {
				assert.ifError(err);
				expect(u.length).equal(1);
				u = u[0];
				expect(u.username).equal(TEST_USER.username);
				expect(u.hashedPassword).to.exist;
				expect(u.salt).to.exist;
				expect(u.created).to.exist;
				done();
			})
		})
	});

	it( 'create the same user, expected error 11000', async () => {

		const mongoose = require('../mongoose').mongoose;

		const TEST_USER = {
			username: 'Test2',
			password: 'password2'
		}

		var user = new User( TEST_USER );
		await user.save( (err) => {
			assert.ifError(err);
		});

		user = new User( TEST_USER );
		await user.save( (err) => {
			expect(err.code).equal(11000);
		});
	});

	it( 'authorize user', async () => {

		const mongoose = require('../mongoose').mongoose;

		const TEST_USER = {
			username: 'Test3',
			password: 'password3'
		}

		var user = new User( TEST_USER );

		await user.save(  function (err) {
			assert.ifError(err);
		});

		var u = await User.authorize( TEST_USER.username, TEST_USER.password );
		expect(u).to.have.property('id');
		expect(u.id).to.be.a('string');
		expect(u.username).equal(TEST_USER.username);
	});

	it( 'authorize user failed', async () => {

		const mongoose = require('../mongoose').mongoose;

		const TEST_USER = {
			username: 'TestFailed',
		}

		var u = await User.authorize( TEST_USER.username, TEST_USER.password );
		expect(u).to.be.a('null');
	});
});
