
require('regenerator-runtime/runtime');

const crypto = require('crypto');
const mongoose = require('../mongoose').mongoose;
const	Schema = mongoose.Schema;

var schema = new Schema({
	username: {
		type: String,
		unique: true,
		required: true
	},
	hashedPassword: {
		type: String,
		required: true
	},
	salt: {
		type: String,
		required: true
	},
	created: {
		type: Date,
		default: Date.now
	}
});

schema.virtual('password')
	.set(function(password) {
		this._plainPassword = password;
		this.salt = Math.random() + '';
		this.hashedPassword = this.encryptPassword(password);
	})
	.get(function() {
		return this._plainPassword;
	});

schema.methods.encryptPassword = function(password) {
	return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
};

schema.methods.checkPassword = function(password)  {
	return this.encryptPassword(password) === this.hashedPassword;
};

schema.statics.authorize = async function(username, password) {
	if( mongoose )
	if( ! username )
		return null;

	const user = await User.findOne( {username: username} );
	if( user )
		return {
			id: user._id.toString(),
			username: user.username
		};

	return null;
}

export const User = mongoose.model('User', schema);
