
'use strict';

require('regenerator-runtime/runtime');

const config = require('./config')();
const webpack = require('webpack');
const logger = require('./logger').log;

console.log( '==> Development configuration...');
const webpackKoaMiddleware = require('koa-webpack-middleware')
const webpackCfg = require('./../webpack.config.js')(config.env);
const compiler = webpack(webpackCfg);

module.exports.use = function(app) {
    app.use(webpackKoaMiddleware.devMiddleware(compiler, {
			path: '/__webpack_hmr',
			publicPath: webpackCfg.output.publicPath,
			// serverSideRender: true,
			stats: {
		    colors: true,
				hash: false,
				timings: true,
				chunks: false,
				chunkModules: false,
				modules: false
			}
    }));
    app.use(webpackKoaMiddleware.hotMiddleware(compiler, {
        // log: console.log,
        // path: '/__webpack_hmr',
        // heartbeat: 10 * 1000,
        reload: true
    }));
}