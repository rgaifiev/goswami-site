
module.exports = {
  defaultTitle: 'Default title',
  pageTitle: 'Page title',
	file: 'en.js',

	loginPage: {
		loginTitle: 'Login',
		loginSubtitle: 'Please enter your username and password (login only for administrator staff)',
		userName: 'User name',
		password: 'Password',
		remember: 'Remember me',
		loginButton: 'Login',
		loginFacebook: 'Login with Facebook',
		loginGoogle: 'Login with Google+',
		loginVK: 'Login with VKontakte',
		registerNow: 'Register Now!',
		forgotPassword: 'Forgot Password?'
	}
};

