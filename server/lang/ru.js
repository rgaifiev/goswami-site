
module.exports = {
  defaultTitle: 'Заголовок',
  pageTitle: 'Заголовок страницы',
  file: 'ru.js',

	loginPage: {
		loginTitle:     'Вход',
		loginSubtitle:  'Please enter your username and password (login only for administrator staff)',
		userName:       'Имя пользователя',
		password:       'Пароль',
		remember:       'Запомнить меня',
		loginButton:    'Войти',
		loginFacebook:  'Войти с помощью Facebook',
		loginGoogle:    'Войти с помощью Google+',
		loginVK:        'Войти с помошью ВКонтакте',
		registerNow:    'Register Now!',
		forgotPassword: 'Забыли пароль?'
	}

}
