const acceptLanguageParser = require('accept-language-parser');
const path = require('path');

export const LANGUAGE = {
    "ru":     require('./ru'),
    "ru-RU":  require('./ru'),
    "en":     require('./en')
};

export function translationStaticFileHandler(fileName) {
    const name = path.parse(fileName).name;
    const tr = JSON.stringify(LANGUAGE[name], '', 4);
    return `window.translations = ${tr}`;
};

const DEFAULT_PROPERTIES = {
	defaultLanguage: 'en',
	acceptedLanguage: null
};

export class TranslationClass {

    constructor( props ) {
        props = props || DEFAULT_PROPERTIES;
        this.defaultLanguage = props.defaultLanguage;

      this.acceptedLanguage = acceptLanguageParser.parse( props.acceptedLanguage );
    }

    getAcceptedLanguageTranslation() {
        if( this.acceptedLanguage )
            return this.acceptedLanguage.length > 0 ? LANGUAGE[formatLang( this.acceptedLanguage[0].code, this.acceptedLanguage[0].region )] : null;
        return null;
    }

    getDefaultTranslation() {
        return LANGUAGE[ this.defaultLanguage ];
    }

    getLang() {
        if( this.acceptedLanguage && this.acceptedLanguage.length > 0 )
            return formatLang( this.acceptedLanguage[0].code, this.acceptedLanguage[0].region);

        return this.defaultLanguage;
    }
}

function formatLang( code, region ) {
    if( ! code )
        return null;
    return code + (region ? '-' + region : '');
}

