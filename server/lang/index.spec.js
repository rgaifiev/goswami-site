import {TranslationClass} from '../../server/lang';
import {expect} from 'chai';


describe( 'Translation', () => {

	it( 'create without accepted-language', () => {
		let translationClass = new TranslationClass();
		expect( translationClass.acceptedLanguage ).to.deep.equal([])
	})

	it( 'parse accepted-language', () => {
		const acceptedLanguage = 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4';
		let translationClass = new TranslationClass( {acceptedLanguage: acceptedLanguage} );
		expect( translationClass.acceptedLanguage.length ).to.equal(4)
		expect( translationClass.acceptedLanguage[0] ).to.deep.equal({
			"code": "ru",
			"quality": 1,
			"region": "RU",
			"script": null
		});
	})

});
