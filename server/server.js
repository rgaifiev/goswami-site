/**
 * Created by rgaifiev_usr on 17.01.2017.
 */

require("babel-register");

// Suppress DEBUG_FD warning
//delete process.env['DEBUG_FD'];

const config = require('./config')();

/**
 *  Logger
 *
 */
const logger = require('./logger').log;

/**
 *  Koa
 *
 */
const Koa = require('koa');
const Router = require('koa-router');

const app = new Koa();

/**
 * Session
 */
const session = require("koa-session-minimal");
const MongoSessionStorage = require('koa-generic-session-mongo');

app.use(session({
	store: new MongoSessionStorage( {
		url: config.mongoose.uri,
		ttl: 1000*60*60*24,
	}),
	key: config.session.key,
	cookie: {
		maxAge: 0
	}
}));

app.on('error', function(err){
  logger.warn('server error', err);
	console.error(err);
});

// Configuring routing
app.use(async (ctx, next) => {
	try {
		await next(); // next is now a function
	} catch (err) {
		console.error(err);
		ctx.body = { message: err.message };
		ctx.status = err.status || 500;
	}
});


/**
 *  Static assets
 */
const translationStaticFileHandler = require('./lang/index').translationStaticFileHandler;
const router = new Router();

const kstatic = require('./modules/koa-static');
router.get( '/assets/lang/:file', kstatic('./server/lang', { param: 'file', handler: translationStaticFileHandler } )  );
app.use( router.routes() );
app.use( require('./services').route );

/**
 *  Webpack
 *
 */
if( config.env === 'development' ) {
    require( './wp-server' ).use(app);
} else {
    console.log( '==> Production configuration...');
}

const reactMiddleware = require('./reactMiddleware');
app.use(reactMiddleware.route);

/**
 *  finally response, if did not route
 */
app.use(async (ctx ) => {
	ctx.status = 404;
	ctx.body = 'Not found';
});

//
// Start mongdb
//
require('./mongoose').connect();

//
// Start listeners
//
const http = require('http');
//const http = require('http2');

http.createServer(app.callback()).listen(config.port, function(err) {
    if (err) {
        logger.error(err);
    }
    console.info('==> 🌎 Listening on port %s. Open up http://%s:%s/ in your browser.', config.port, config.hostname, config.port);
});

