
import {renderToString} from 'react-dom/server';
import React from 'react';
import {StaticRouter, Route} from 'react-router';
import {SheetsRegistryProvider, SheetsRegistry} from 'react-jss'
import {Provider} from 'react-redux'

import {DefaultTheme} from '../app/theme/OrangeTheme'
import Root from '../app/pages/Root'

import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {getMetaTagsRender} from '../app/utils/meta-tags-render'
import {TranslationClass} from './lang';

import {initStore} from '../app/store'

require('../app/utils/injectTapEventPlugin');

module.exports.route = async ( ctx, next ) => {

    const store = initStore();
    const context = {};

    let translationClass = new TranslationClass( {acceptedLanguage: ctx.req.headers['accept-language']} );
    let appTheme = getMuiTheme(DefaultTheme, { userAgent: ctx.req.headers['user-agent'] });
    const sheets = new SheetsRegistry();

    const translation = translationClass.getAcceptedLanguageTranslation() || translationClass.getDefaultTranslation();
    let markup = renderToString(
        <StaticRouter
            location={ctx.url}
            context={context}
        >
	        <Provider store={store}>
            <SheetsRegistryProvider registry={sheets}>
                <Root translations={translation} theme={appTheme}/>
            </SheetsRegistryProvider>
          </Provider>
        </StaticRouter>
    )
		if( markup.indexOf('NOT_MATCH_01234567') === -1 ) {
			let metaTags = getMetaTagsRender();

			// const stat = ctx.locals.webpackStats.toJson();
			// console.log(stat);
			ctx.status = 200;
			ctx.set('Content-Type', 'text/html');
			let content = `
<html>
    <head>
        <meta charset="utf-8">
        <title>${metaTags.getTitle()}</title>
        
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
        
        <script src="/assets/bundle.commons.js"></script>
        <script src="/assets/bundle.vendor.js"></script>
        <script src="/assets/lang/${translation.file}"></script>
        
        <style type="text/css" data-jss>
            ${sheets.toString()}
        </style>
    </head>
    <body>
        <div id="content-root">${markup}</div>
        <script src="/assets/bundle.main.js"></script>
    </body>
</html>`;

			ctx.body = `<!DOCTYPE html>${content}`;
		} else
			next();
};
