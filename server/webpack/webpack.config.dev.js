
'use strict';

const path = require('path' );
const webpack = require('webpack');

const root_path = path.resolve(__dirname, '../../' );

module.exports = env => { return {
    context: root_path,
    entry: {
        vendor: [
            'babel-polyfill'
        ],
        main: [
            'webpack-hot-middleware/client',
            './app/index.jsx'
        ]
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'bundle.commons',
            filename: 'bundle.commons.js',
            minChunks: 2
        }),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify(env)
            }
        })
    ],
    module: {
        rules: [
            {
                test: /\.jsx$/,
                use: {
                    loader: 'babel-loader'
                },
                exclude: /node_modules/
            }
        ]
    },
    devtool: 'eval',
    output: {
        path: path.resolve(root_path, './dist'),
        publicPath: "/assets/",
        filename: 'bundle.[name].js',
    },
}}