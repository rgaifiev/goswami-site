
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;

exports.mongoose = mongoose;
exports.connect = (env) => {
	const config = require('./config')(env);
	mongoose.connect( config.mongoose.uri );
	mongoose.connection.on('connected', function () {
		console.log('==> 🌎 Connected to MongoDB (%s): %s', config.env, config.mongoose.uri);
	});

	mongoose.connection.on('error', function (err) {
		throw err;
	});

	return mongoose;
}
