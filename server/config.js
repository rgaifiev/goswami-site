/**
 * Created by rgaifiev_usr on 17.01.2017.
 */

const CONFIG = {
  development: {
    env:       'development',
    port:       3000,
    hostname:   'localhost',
	  session: {
    	key: 'goswamiSid'
	  },
		mongoose: {
      uri: "mongodb://localhost/goswami",
			options: {
        server: {
          socketOptions: {
            keepAlive: 1
		      }
	      }
			}
		}
  },
	test: {
		env:       'test',
		port:       3000,
		hostname:   'localhost',
		session: {
			key: 'goswamiSid'
		},
		mongoose: {
			uri: "mongodb://localhost/test",
			options: {
				server: {
					socketOptions: {
						keepAlive: 1
					}
				}
			}
		}
	}
}

module.exports = function(env) {
    const e = env || process.env.NODE_ENV || 'development';
    const config = CONFIG[e];
    if( ! config )
        throw new Error( 'Cannot get configuration for "' + env + '"' );
    return config;
}

