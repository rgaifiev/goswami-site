/**
 * Created by rgaifiev_usr on 17.01.2017.
 */

const bunyan = require('bunyan');
const logger = bunyan.createLogger({name: "goswami.ru"});

module.exports = {
    log: logger
}
