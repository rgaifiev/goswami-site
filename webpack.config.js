/**
 * Created by rgaifiev_usr on 17.01.2017.
 */

'use strict';

module.exports = env => {
    const e = env || 'development';
    return e === 'production'
        ? require('./server/webpack/webpack.config.dev.js')(e)
        : require('./server/webpack/webpack.config.dev.js')(e);
};